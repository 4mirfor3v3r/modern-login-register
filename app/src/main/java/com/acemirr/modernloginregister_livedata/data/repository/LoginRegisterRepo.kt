package com.acemirr.modernloginregister_livedata.data.repository

import com.acemirr.modernloginregister_livedata.data.ApiHelper
import com.acemirr.modernloginregister_livedata.data.model.ResponseUsers
import io.reactivex.Single

class LoginRegisterRepo(private val apiHelper: ApiHelper) {
    fun login(email:String,password:String): Single<ResponseUsers> {
        return apiHelper.login(email,password)
    }
    fun register(name:String, email: String, password: String): Single<ResponseUsers> {
        return apiHelper.register(name,email,password)
    }
    fun verifyUsername(username:String):Single<ResponseUsers>{
        return apiHelper.verifyUsername(username)
    }
}