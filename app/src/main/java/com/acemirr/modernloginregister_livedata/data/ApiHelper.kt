package com.acemirr.modernloginregister_livedata.data

class ApiHelper(private val apiService: ApiService) {
    fun login(email:String,password:String) = apiService.login(email, password)
    fun register(username:String, email:String, password:String) = apiService.register(username, email, password)
    fun verifyUsername(username: String) = apiService.verifyUsername(username)
}