package com.acemirr.modernloginregister_livedata.data.model

data class ResponseUsers (
    var status:String?,
    var msg:String?,
    var user: User?
)