package com.acemirr.modernloginregister_livedata.data.model

data class User(
    var _id:String,
    var username:String,
    var email:String,
    var password:String
)