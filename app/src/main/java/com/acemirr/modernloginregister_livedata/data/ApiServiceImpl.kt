package com.acemirr.modernloginregister_livedata.data

import com.acemirr.modernloginregister_livedata.data.model.ResponseUsers
import io.reactivex.Single

class ApiServiceImpl:ApiService {
    override fun verifyUsername(username: String): Single<ResponseUsers> {
        return NetworkConfig.api().verifyUsername(username)
    }

    override fun register(username: String, email: String, password: String): Single<ResponseUsers> {
        return NetworkConfig.api().register(username, email, password)
    }

    override fun login(email: String, password: String): Single<ResponseUsers> {
        return NetworkConfig.api().login(email, password)
    }
}