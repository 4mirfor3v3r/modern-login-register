package com.acemirr.modernloginregister_livedata.data

import com.acemirr.modernloginregister_livedata.data.model.ResponseUsers
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {
    @GET("users/verify-username/{username}")
    fun verifyUsername(@Path("username") username:String) : Single<ResponseUsers>

    @FormUrlEncoded
    @POST("users/register")
    fun register(
        @Field("username") username: String,
        @Field("email") email: String,
        @Field("password") password: String
    ) : Single <ResponseUsers>

    @FormUrlEncoded
    @POST("users/login")
    fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ) : Single <ResponseUsers>
}
