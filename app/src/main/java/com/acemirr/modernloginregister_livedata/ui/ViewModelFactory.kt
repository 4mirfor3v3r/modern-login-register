package com.acemirr.modernloginregister_livedata.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.acemirr.modernloginregister_livedata.data.ApiHelper
import com.acemirr.modernloginregister_livedata.data.repository.LoginRegisterRepo
import com.acemirr.modernloginregister_livedata.ui.login.LoginViewModel
import com.acemirr.modernloginregister_livedata.ui.register.RegisterViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val apiHelper: ApiHelper): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> LoginViewModel(LoginRegisterRepo(apiHelper)) as T
            modelClass.isAssignableFrom(RegisterViewModel::class.java) -> RegisterViewModel(LoginRegisterRepo(apiHelper)) as T
            else -> throw IllegalArgumentException("Unknown Class Name")
        }
    }
}