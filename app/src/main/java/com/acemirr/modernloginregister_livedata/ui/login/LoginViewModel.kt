package com.acemirr.modernloginregister_livedata.ui.login

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.acemirr.modernloginregister_livedata.data.model.ResponseUsers
import com.acemirr.modernloginregister_livedata.data.repository.LoginRegisterRepo
import com.acemirr.modernloginregister_livedata.utils.Resource
import com.acemirr.modernloginregister_livedata.utils.SingleLiveData
import com.acemirr.modernloginregister_livedata.utils.validateEmail
import com.acemirr.modernloginregister_livedata.utils.validatePassword
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LoginViewModel(private val loginRegisterRepo: LoginRegisterRepo) : ViewModel() {
    var email: MutableLiveData<String> = MutableLiveData()
    var password: MutableLiveData<String> = MutableLiveData()
    var isValid: MutableLiveData<Boolean> = MutableLiveData()

    var errorEmail: MutableLiveData<String> = MutableLiveData()
    var errorPassword: MutableLiveData<String> = MutableLiveData()

    private var isEmailValid: Boolean = false
    private var isPasswordValid: Boolean = false

    val uiEventData = SingleLiveData<Int>()
    private val user = MutableLiveData<Resource<ResponseUsers>>()
    private val compositeDisposable = CompositeDisposable()

    fun setupLiveData(lifecycleOwner: LifecycleOwner, context: Context) {
        email.observe(lifecycleOwner, Observer { email ->
            val validationModel = email.validateEmail(context)
            isEmailValid = validationModel.isValid
            validateInput(isEmailValid, isPasswordValid)
            errorEmail.postValue(validationModel.message)
        })
        password.observe(lifecycleOwner, Observer { password ->
            val validationModel = password.validatePassword(context)
            isPasswordValid = validationModel.isValid
            validateInput(isEmailValid, isPasswordValid)
            errorPassword.postValue(validationModel.message)
        })
    }
    private fun validateInput(email: Boolean, password: Boolean) {
        isValid.postValue(email && password)
    }

    private fun verify(email:String, password:String){
        user.postValue(Resource.loading(null))
        compositeDisposable.add(
            loginRegisterRepo.login(email,password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    user.postValue(Resource.success(it))
                },{
                    user.value = Resource.error("Something went Wrong ${it.message}",null)
                })
        )
    }
    fun performLogin(){
        verify(email.value!!,password.value!!)
    }

    fun onClickEvent(value:Int){
        uiEventData.setValue(value)
    }

    fun getUsers() = user

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}