package com.acemirr.modernloginregister_livedata.ui.register

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.acemirr.modernloginregister_livedata.data.model.ResponseUsers
import com.acemirr.modernloginregister_livedata.data.repository.LoginRegisterRepo
import com.acemirr.modernloginregister_livedata.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RegisterViewModel(private val loginRegisterRepo: LoginRegisterRepo) : ViewModel() {
    var username:MutableLiveData<String> = MutableLiveData()
    var email: MutableLiveData<String> = MutableLiveData()
    var password: MutableLiveData<String> = MutableLiveData()

    var errorUsername: MutableLiveData<String> = MutableLiveData()
    var errorEmail: MutableLiveData<String> = MutableLiveData()
    var errorPassword: MutableLiveData<String> = MutableLiveData()
    var helperTextUsername:MutableLiveData<String> = MutableLiveData()

    private var isUsernameValid: Boolean = false
    private var isEmailValid: Boolean = false
    private var isPasswordValid: Boolean = false
    var isValid: MutableLiveData<Boolean> = MutableLiveData()
    private var isUsernameLoading:MutableLiveData<Boolean> = MutableLiveData()

    private val compositeDisposable = CompositeDisposable()
    private val user = MutableLiveData<Resource<ResponseUsers>>()
    val uiEventData = SingleLiveData<Int>()

    private fun verifyUsername(string: String){
        isUsernameLoading.postValue(true)
        isUsernameValid = false
        errorUsername.postValue("Checking username")
        validateInput(isUsernameValid,isEmailValid,isPasswordValid)
        compositeDisposable.add(
            loginRegisterRepo.verifyUsername(string)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status == "ok"){
                        isUsernameValid = true
                        helperTextUsername.postValue(it.msg)
                    }
                    else if (it.status == "error"){
                        isUsernameValid = false
                        errorUsername.postValue(it.msg)
                    }
                    isUsernameLoading.postValue(false)
                    validateInput(isUsernameValid,isEmailValid,isPasswordValid)
                },{
                    isUsernameValid = false
                    validateInput(isUsernameValid,isEmailValid,isPasswordValid)
                        if (it.cause?.message != null && it.cause?.message?.contains("No address associated with hostname")!!) {
                            errorUsername.postValue("No Internet Connection")
                            Log.e("CAUSE ", it.cause?.message.toString())
                        }
                    else {
                            errorUsername.postValue("Something went Wrong \n${it.message}")
                        }
                    isUsernameLoading.postValue(false)
                })
        )
    }
    private fun register(username: String, email:String, password: String){
        compositeDisposable.add(
            loginRegisterRepo.register(username,email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    user.postValue(Resource.success(it))
                },{
                    user.value = Resource.error("Something went Wrong ${it.message}",null)
                })
        )
    }
    fun setupLiveData(lifecycleOwner: LifecycleOwner, context: Context) {
        username.observe(lifecycleOwner, Observer { username ->
            username.validateUsername(context){
                if (it.isValid){
                    if (it.message.length >= 3){
                        Log.e("VALID = TRUE",it.message)
                        verifyUsername(it.message)
                    }
                    else{
                        compositeDisposable.clear()
                        isUsernameValid = false
                    }
                }else{
                    compositeDisposable.clear()
                    isUsernameValid = false
                    errorUsername.postValue(it.message)
                    Log.e("ERRORUSERNAME"," s "+it.message)
                }
                validateInput(isUsernameValid,isEmailValid,isPasswordValid)
            }

        })
        email.observe(lifecycleOwner, Observer { email ->
            val validationModel = email.validateEmail(context)
            isEmailValid = validationModel.isValid
            validateInput(isUsernameValid, isEmailValid, isPasswordValid)
            errorEmail.postValue(validationModel.message)
        })

        password.observe(lifecycleOwner, Observer { password ->
            val validationModel = password.validatePassword(context)
            isPasswordValid = validationModel.isValid
            validateInput(isUsernameValid, isEmailValid, isPasswordValid)
            errorPassword.postValue(validationModel.message)
        })
    }

    private fun validateInput(username: Boolean, email: Boolean, password: Boolean) {
        isValid.postValue(username && email && password)
    }

    fun onClickEvent(value:Int){
        uiEventData.setValue(value)
    }

    fun getUsers() = user

    fun performRegister() {
        register(username.value!!,email.value!!,password.value!!)
    }
}