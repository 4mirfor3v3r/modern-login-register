package com.acemirr.modernloginregister_livedata.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.acemirr.modernloginregister_livedata.R
import com.acemirr.modernloginregister_livedata.data.ApiHelper
import com.acemirr.modernloginregister_livedata.data.ApiServiceImpl
import com.acemirr.modernloginregister_livedata.databinding.ActivityLoginBinding
import com.acemirr.modernloginregister_livedata.ui.ViewModelFactory
import com.acemirr.modernloginregister_livedata.ui.register.RegisterActivity
import com.acemirr.modernloginregister_livedata.utils.Status

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)

        viewModel = ViewModelProvider(this,ViewModelFactory(ApiHelper(ApiServiceImpl()))).get(LoginViewModel::class.java)
        binding.vm = viewModel
        viewModel.setupLiveData(this, this)

        listenClick()
    }

    private fun listenClick(){
        viewModel.uiEventData.observe(this, Observer {
            when(it){
                0 -> {
                    startActivity(Intent(this,RegisterActivity::class.java))
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                }
                1-> viewModel.performLogin()
            }
        })

        viewModel.getUsers().observe(this, Observer {
            when(it.status){
                Status.SUCCESS->{
                    Toast.makeText(this,"Login Success",Toast.LENGTH_SHORT).show()
                }
                Status.ERROR ->{
                    Toast.makeText(this,"Email or password wrong",Toast.LENGTH_SHORT).show()
                }
                Status.LOADING ->{
                    binding.textInputPassword.editText?.setText("")
                }
            }
        })
    }
}
