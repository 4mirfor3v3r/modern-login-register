package com.acemirr.modernloginregister_livedata.ui.register

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.acemirr.modernloginregister_livedata.R
import com.acemirr.modernloginregister_livedata.data.ApiHelper
import com.acemirr.modernloginregister_livedata.data.ApiServiceImpl
import com.acemirr.modernloginregister_livedata.databinding.ActivityRegisterBinding
import com.acemirr.modernloginregister_livedata.ui.ViewModelFactory
import com.acemirr.modernloginregister_livedata.utils.Status

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)

        viewModel = ViewModelProvider(this, ViewModelFactory(ApiHelper(ApiServiceImpl()))).get(RegisterViewModel::class.java)
        binding.vm = viewModel
        viewModel.setupLiveData(this, this)

        listenClick()
    }

    override fun onBackPressed() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
        super.onBackPressed()
    }
    private fun listenClick(){
        viewModel.uiEventData.observe(this, Observer {
            when(it){
                0 -> {
                    onBackPressed()
                }
                1 -> {
                    viewModel.performRegister()
                }
            }
        })
        viewModel.getUsers().observe(this, Observer {
            when(it.status){
                Status.SUCCESS->{
                    binding.textInputUsername.editText?.setText("")
                    binding.textInputEmail.editText?.setText("")
                    binding.textInputPassword.editText?.setText("")
                    Toast.makeText(this,"Register Success",Toast.LENGTH_LONG).show()
                }
                Status.ERROR ->{
                    Toast.makeText(this,"Something went wrong ${it.message}",Toast.LENGTH_SHORT).show()
                }
                Status.LOADING ->{
                }
            }
        })
    }

}
