package com.acemirr.modernloginregister_livedata.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}