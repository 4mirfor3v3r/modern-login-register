package com.acemirr.modernloginregister_livedata.utils

import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("errorText")
fun TextInputLayout.setErrorMessage(errorMessage: String?) {
    this.error = errorMessage
}
@BindingAdapter("helperText")
fun TextInputLayout.setHelperTextMessage(successMessage:String?){
    this.isHelperTextEnabled = true
    this.helperText = successMessage
}