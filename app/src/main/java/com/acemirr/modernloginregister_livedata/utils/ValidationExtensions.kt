package com.acemirr.modernloginregister_livedata.utils

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.Patterns
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.acemirr.modernloginregister_livedata.R
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class ValidationModel(val isValid: Boolean, val message: String)

fun String.validateEmail(context: Context): ValidationModel {
    return if (this.trim().isEmpty()) {
        ValidationModel(false, context.getString(R.string.validation_email_empty))
    } else if (!Patterns.EMAIL_ADDRESS.matcher(this).matches()) {
        ValidationModel(false, context.getString(R.string.validation_email_not_valid))
    } else {
        ValidationModel(true, "")
    }
}

fun String.validatePassword(context: Context): ValidationModel {
    val pattern = "[0-9]".toRegex()
    val pattern2 = "[a-z]|[A-Z]".toRegex()

    return if (this.isEmpty()) {
        ValidationModel(false, context.getString(R.string.validation_password_empty))
    } else if (this.length < 5) {
        ValidationModel(false, context.getString(R.string.validation_password_min))
    } else if (!pattern.containsMatchIn(this) || !pattern2.containsMatchIn(this)) {
        ValidationModel(false, context.getString(R.string.validation_password_wrong))
    } else {
        ValidationModel(true, "")
    }
}

var mObservable: PublishSubject<String> = PublishSubject.create()
var temp = "...."
fun String.validateUsername(context: Context, result: (ValidationModel) -> Unit) {
    val pattern = "[a-z]|[A-Z]|[0-9]|_|-".toRegex()
    val str = this
    mObservable.onNext(this)
    when {
        str.trim().isEmpty() -> result(ValidationModel(false, context.getString(R.string.validation_username_empty)))
        !pattern.containsMatchIn(str) -> result(ValidationModel(false,context.getString(R.string.validation_username_type_error)))
        str.length < 3 -> result(ValidationModel(false, context.getString(R.string.validation_username_min)))
        str.length >=3 -> {
            Log.e("EXECUTED","TRUE")
                mObservable
                    .debounce(400, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .subscribeOn(Schedulers.io())
                    .subscribe { s ->
                        if (s != temp) {
                            Log.e("TEMP 1", temp + "NULL")
                            temp = s
                            Log.e("TEMP 2", temp)
                            result(ValidationModel(true, temp))
                        }
                    }
        }
    }
}

